use serde::{Serialize, Deserialize};
use ring::digest;
use bincode;

#[derive(Serialize, Deserialize, Debug)]
struct NameHash {
    name: String,
    hash: String,
}

fn main() {
    println!("Hello, world!");

    let name = format!("Siwei Xian");

    // println!("{}", name);

    let b = (&name).as_bytes();

    // println!("{:?}", b);

    let sha_digest = digest::digest(&digest::SHA256, b);
    let s = sha_digest.as_ref();

    // println!("{:?}", s);

    let hash = hex::encode(s);

    // println!("{:?}", hash);

    let my_name_hash = NameHash {name, hash};

    // println!{"{:?}", my_name_hash};

    let serialized = bincode::serialize(&my_name_hash).unwrap();

    println!{"{:?}", serialized};

    let deserialized: NameHash = bincode::deserialize(&serialized).unwrap();

    println!{"{:?}", deserialized   };
}
