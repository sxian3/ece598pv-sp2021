use serde::{Serialize,Deserialize};
// use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters, UnparsedPublicKey, ED25519};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, UnparsedPublicKey, ED25519};
use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;
use bincode;
use crate::crypto::hash::{H160, H256, Hashable};
use crate::block::{Block, Header, Content};
use std::collections::{VecDeque, HashMap};

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Transaction {
    pub from: H160,
    pub nonce: u32,
    pub to: H160,
    pub value: u32,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct SignedTransaction {
    pub transaction: Transaction,
    pub signature: Vec<u8>,
    pub public_key: Vec<u8>,
}

impl Hashable for Transaction {
    fn hash(&self) -> H256 {
        let se_t = bincode::serialize(&self).unwrap();
        return ring::digest::digest(&ring::digest::SHA256, &se_t).into();
    }
}

impl Hashable for SignedTransaction {
    fn hash(&self) -> H256 {
        let se_t = bincode::serialize(&self).unwrap();
        return ring::digest::digest(&ring::digest::SHA256, &se_t).into();
    }
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let se_t = bincode::serialize(t).unwrap();
    let sig = key.sign(&se_t);
    return sig;
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    let se_t = bincode::serialize(t).unwrap();
    let k = UnparsedPublicKey::new(&ED25519, public_key.as_ref());

    k.verify(&se_t, signature.as_ref()).is_ok()
}

pub fn verify_bytes(t: &Transaction, public_key_bytes: &Vec<u8>, signature_bytes: &Vec<u8>) -> bool {
    let se_t: Vec<u8> = bincode::serialize(&t).unwrap();
    let k = UnparsedPublicKey::new(&ED25519, public_key_bytes);
    
    k.verify(&se_t[..], signature_bytes).is_ok()
}

/* Mempool */
#[derive(Debug, Default, Clone)]
pub struct Mempool{
    // pub queue: VecDeque<H256>,
    pub nonce_map: HashMap<(H160, u32), H256>,
    pub tx_map: HashMap<H256, SignedTransaction>,
}

impl Mempool {
    pub fn new() -> Self {
        return Mempool{nonce_map: HashMap::new(), tx_map: HashMap::new()}  
    }

    pub fn insert(&mut self, t: &SignedTransaction) {
        let t_hash = t.hash();
        let acc_nonce = (t.transaction.from, t.transaction.nonce);
        // reject double spend in mempool
        if self.tx_map.contains_key(&t_hash) || self.nonce_map.contains_key(&acc_nonce) {
            return;
        }
        self.nonce_map.insert(acc_nonce, t_hash);
        self.tx_map.insert(t_hash, t.clone());
    }

    pub fn remove(&mut self, t: &SignedTransaction) {
        let t_hash = t.hash();
        let nonce = (t.transaction.from, t.transaction.nonce);
        if (self.nonce_map.contains_key(&nonce)){
            self.nonce_map.remove(&nonce);
        }
        if (self.tx_map.contains_key(&t_hash)) {
            self.tx_map.remove(&t_hash);
        }
    }
}

/* State */
#[derive(Debug, Default, Clone)]
pub struct StatePerBlock {
    pub map: HashMap<H256, State>,
}

impl StatePerBlock {
    pub fn new() -> Self {
        return StatePerBlock{map: HashMap::new()};
    }

    pub fn ico(&mut self, h: H256) {
        let seed_0 = [0u8; 32];
        let key_0 = Ed25519KeyPair::from_seed_unchecked(&seed_0).unwrap();
        let public_key_0 = key_0.public_key();
        let pkhash_0: H256 = ring::digest::digest(&ring::digest::SHA256, public_key_0.as_ref()).into();
        let addr_0: H160 = pkhash_0.to_addr().into();

        let mut seed_1 = [0u8; 32];
        seed_1[0] = 1u8;
        let key_1 = Ed25519KeyPair::from_seed_unchecked(&seed_1).unwrap();
        let public_key_1 = key_1.public_key();
        let pkhash_1: H256 = ring::digest::digest(&ring::digest::SHA256, public_key_1.as_ref()).into();
        let addr_1: H160 = pkhash_1.to_addr().into();

        let mut seed_2 = [0u8; 32];
        seed_2[0] = 2u8;
        let key_2 = Ed25519KeyPair::from_seed_unchecked(&seed_2).unwrap();
        let public_key_2 = key_2.public_key();
        let pkhash_2: H256 = ring::digest::digest(&ring::digest::SHA256, public_key_2.as_ref()).into();
        let addr_2: H160 = pkhash_2.to_addr().into();

        let mut ico_state = State::new();
        ico_state.state.insert(addr_0, (0, 10000));
        ico_state.state.insert(addr_1, (0, 10000));
        ico_state.state.insert(addr_2, (0, 10000));
        self.map.insert(h, ico_state);
    }

    pub fn update(&mut self, tip: H256, block: &Block) {
        let mut new_state = self.map[&tip].clone();
        let transactions = block.clone().content.content;

        for t in &transactions {
            new_state.update(&t);
        }

        self.map.insert(block.hash(), new_state);
    }
}

#[derive(Debug, Default, Clone)]
pub struct State {
    pub state: HashMap<H160, (u32, u32)>,
}

impl State {
    pub fn new() -> Self {
        return State{state: HashMap::new()}
    }

    pub fn update(&mut self, t: &SignedTransaction) -> bool {
        let tx = t.transaction.clone();
        let from = tx.from;
        let nonce = tx.nonce;
        let to = tx.to;
        let value = tx.value;

        let new_send_nonce = self.state[&from].0 + 1;
        
        // reject double spend or overspend
        if nonce != new_send_nonce || self.state[&from].1 < value {
            println!("can't update invalid tx");
            return false;
        }
        
        let new_send_balance = self.state[&from].1 - value;
        
        self.state.insert(from, (new_send_nonce, new_send_balance));

        let mut new_recv_nonce = 0;
        let mut new_recv_balance = value;
        if self.state.contains_key(&to) {
            new_recv_nonce = self.state[&to].0;
            new_recv_balance = new_recv_balance + self.state[&to].1;
        }

        self.state.insert(to, (new_recv_nonce, new_recv_balance));

        return true;
    }
    // pub fn validate_transaction(&self, t: &SignedTransaction) -> bool {
    //     let tx = t.transaction.clone();
    //     let from = tx.from;
    //     let nonce = tx.nonce;
    //     let _nonce = nonce + 1;
    //     let to = tx.to;
    //     let value = tx.value;

    //     if self.state.contains_key(&from) {
    //         return self.state[&from].0 == _nonce && self.state[&from].1 > value;
    //     }

    //     return false;
    // }
}


#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        // Default::default()
        unimplemented!()
        // let mut rng = rand::thread_rng();

        // let input: String = rng
        //     .sample_iter(&Alphanumeric)
        //     .take(7)
        //     .map(char::from)
        //     .collect();

        // let output: String = rng
        //     .sample_iter(&Alphanumeric)
        //     .take(7)
        //     .map(char::from)
        //     .collect();

        // let ret = Transaction {input, output};
        // return ret;
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
        let key_2 = key_pair::random();
        assert!(verify(&t, &(key_2.public_key()), &signature) == false);
    }

    #[test]
    fn assignment2_transaction_1() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
    
    #[test]
    fn assignment2_transaction_2() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        let key_2 = key_pair::random();
        let t_2 = generate_random_transaction();
        assert!(!verify(&t_2, &(key.public_key()), &signature));
        assert!(!verify(&t, &(key_2.public_key()), &signature));
    }
}
