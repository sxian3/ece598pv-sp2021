use super::message::Message;
use super::peer;
use crate::network::server::Handle as ServerHandle;
use crossbeam::channel;
use log::{debug, info, warn};

use std::thread;

use std::sync::{Arc, Mutex};
use std::collections::HashMap;
use crate::block::{Block, Header, Content};
use crate::blockchain::Blockchain;
use crate::crypto::hash::{H160, H256, Hashable};
use crate::transaction::{self, Transaction, SignedTransaction, Mempool, State, StatePerBlock};
use std::time;
use std::process;
use std::fs::File;
use std::path::Path;
use std::io::{Write, LineWriter};

#[derive(Clone)]
pub struct Context {
    msg_chan: channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    buffer: Arc<Mutex<HashMap<H256, Block>>>, // block orphan buffer
    mempool: Arc<Mutex<Mempool>>,
    states: Arc<Mutex<StatePerBlock>>,
}

pub fn new(
    num_worker: usize,
    msg_src: channel::Receiver<(Vec<u8>, peer::Handle)>,
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    buffer: &Arc<Mutex<HashMap<H256, Block>>>,
    mempool: &Arc<Mutex<Mempool>>,
    states: &Arc<Mutex<StatePerBlock>>,
) -> Context {
    Context {
        msg_chan: msg_src,
        num_worker,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        mempool: Arc::clone(mempool),
        buffer: Arc::clone(buffer),
        states: Arc::clone(states),
    }
}

impl Context {
    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let cloned = self.clone();
            thread::spawn(move || {
                // debug!("Worker thread {} started", i);
                cloned.worker_loop();
                warn!("Worker thread {} exited", i);
            });
        }
    }

    fn worker_loop(&self) {
        let mut _worker_start_time = time::SystemTime::now();
        let mut delay_sum = 0;
        let mut num_blocks = 0;

        loop {
            let msg = self.msg_chan.recv().unwrap();
            let (msg, peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();

            // println!("MSG {:?}", msg);

            let mut _blockchain = self.blockchain.lock().unwrap();
            let mut _buffer = self.buffer.lock().unwrap();
            let mut _mempool = self.mempool.lock().unwrap();
            let mut _states = self.states.lock().unwrap();

            match msg {
                Message::Ping(nonce) => {
                    println!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }
                Message::Pong(nonce) => {
                    println!("Pong: {}", nonce);
                }
                Message::NewBlockHashes(hashes) => {
                    let mut unseen = Vec::new();

                    for hash in hashes.iter() {
                        if !_blockchain.blocks.contains_key(hash) {
                            unseen.push(hash.clone());
                        }
                    }

                    if unseen.len() > 0 {
                        peer.write(Message::GetBlocks(unseen.clone()));
                    }
                }
                Message::GetBlocks(hashes) => {
                    let mut requested = Vec::new();

                    for hash in hashes.iter() {
                        if _blockchain.blocks.contains_key(hash) {
                            requested.push(_blockchain.blocks[hash].clone());
                        }
                    }

                    if requested.len() > 0 {
                        peer.write(Message::Blocks(requested));
                    }
                }
                Message::Blocks(blocks) => {
                    let mut v = Vec::new();
                    let mut unseen = Vec::new();

                    for block in blocks.iter() {
                        // Block delay
                        let delay = time::SystemTime::now().duration_since(time::SystemTime::UNIX_EPOCH).unwrap().as_millis() - block.header.timestamp;
                        delay_sum = delay_sum + delay;
                        num_blocks = num_blocks + 1;
                        // info!("size {}, delay {}, avg {}", bincode::serialize(block).unwrap().len(), delay, delay_sum/num_blocks);

                        // Orphan check
                        if !_blockchain.blocks.contains_key(&block.header.parent) {
                            unseen.push(block.header.parent);
                            _buffer.insert(block.header.parent, block.clone());
                        } 
                        
                        // Block Validity check (hash, difficulty)
                        else if block.hash() < block.header.difficulty && _blockchain.blocks[&block.header.parent].header.difficulty == block.header.difficulty {
                            let mut transactions = block.clone().content.content;
                            let mut block_transaction_valid = true;

                            for t in &transactions {
                                // Signature check
                                if !transaction::verify_bytes(&t.transaction, &t.public_key, &t.signature) {
                                    println!("invalid tx");
                                    block_transaction_valid = false;
                                    break;
                                }
                            }

                            // Block Transaction Validity Check
                            if block_transaction_valid {
                                for t in transactions {
                                    _mempool.remove(&t);
                                }

                                _blockchain.insert(block);
                                _states.update(block.header.parent, block);
                                v.push(block.hash());
                                let mut parent_hash = block.hash();

                                // recursively insert earlier received children
                                while _buffer.contains_key(&parent_hash) {
                                    let child_block = _buffer.remove(&parent_hash).unwrap();

                                    transactions = child_block.clone().content.content;
                                    block_transaction_valid = true;

                                    for t in &transactions {
                                        // Signature check
                                        if !transaction::verify_bytes(&t.transaction, &t.public_key, &t.signature) {
                                            println!("invalid tx");
                                            block_transaction_valid = false;
                                            break;
                                        }
                                        println!("valid tx");
                                        // TODO: Double spend check
                                    }

                                    if block_transaction_valid {
                                        for t in transactions {
                                            _mempool.remove(&t);
                                        }

                                        _blockchain.insert(&child_block);
                                        _states.update(parent_hash, &child_block);
                                        v.push(child_block.hash());
                                        parent_hash = child_block.hash();
                                    }
                                }
                            }
                        }
                    }

                    if unseen.len() > 0 {
                        peer.write(Message::GetBlocks(unseen));
                    }
                    if v.len() > 0 {
                        self.server.broadcast(Message::NewBlockHashes(v));
                    }
                }
                Message::NewTransactionHashes(hashes) => {
                    println!{"NewTransactionHashes {:?}", hashes};
                    let mut unknown = Vec::new();

                    for hash in hashes.iter() {
                        if !_mempool.tx_map.contains_key(hash) {
                            unknown.push(hash.clone());
                        }
                    }

                    if unknown.len() > 0 {
                        peer.write(Message::GetTransactions(unknown));
                    }
                }
                Message::GetTransactions(hashes) => {
                    println!{"GetTransactions {:?}", hashes};
                    let mut requested = Vec::new();

                    for hash in hashes.iter() {
                        if _mempool.tx_map.contains_key(hash) {
                            requested.push(_mempool.tx_map[hash].clone());
                        }
                    }

                    if requested.len() > 0 {
                        peer.write(Message::Transactions(requested));
                    }
                }
                Message::Transactions(transactions) => {
                    for t in &transactions {
                        let tx = t.transaction.clone();
                        let from = tx.from;
                        let nonce = tx.nonce;
                        let to = tx.to;
                        let value = tx.value;
                        debug!("Receive {:?}", tx);
                        // Signature check
                        if transaction::verify_bytes(&t.transaction, &t.public_key, &t.signature) {
                            let tx_valid = true;
                            _mempool.insert(&t);

                            // Double spend/Overspend check
                            // if _states[_blockchain.tip].contains_key(&from) {
                            //     let expected_nonce = _states[_blockchain.tip][from].0 + 1;
                            //     let balance = _states[_blockchain.tip][from].1;
                            //     if balance < value || nonce != expected_nonce {
                            //         tx_valid = false;
                            //     }
                            //     _mempool.insert(&t);
                            // }
                            
                        } else {
                            debug!("invalid tx");
                        }
                    }
                }
            }
        }
    }
}
