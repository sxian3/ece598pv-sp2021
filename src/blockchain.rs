use crate::block::{Header, Content, Block};
use crate::crypto::hash::{H256, Hashable};
use rand::Rng;
use std::time::{SystemTime};
use crate::transaction::{Transaction, SignedTransaction};
use crate::crypto::merkle::{MerkleTree};
use std::collections::HashMap;

use log::{info, debug};

pub struct Blockchain {
    pub tip: H256,
    pub blocks: HashMap<H256, Block>,
    pub lengths: HashMap<H256, u32>
}

impl Blockchain {
    /// Create a new blockchain, only containing the genesis block
    pub fn new() -> Self {
        // let mut rng = rand::thread_rng();

        // Parent
        let parent: H256 = [0u8; 32].into();
        // Nonce
        let nonce = 0u32;
        // Difficulty
        let mut _difficulty = [0u8; 32];
        _difficulty[0] = 16u8;
        let difficulty: H256 = _difficulty.into();
        // Timestamp
        let timestamp: u128 = 0u128;
        // Transactions as merkle tree
        let transactions = Vec::<SignedTransaction>::new();
        let merkle_tree = MerkleTree::new(&transactions);
        let merkle_root = merkle_tree.root();

        // Create header, content and block
        let header = Header{parent, nonce, difficulty, timestamp, merkle_root};
        let content = Content{content: Vec::new()};
        let block = Block{header, content};

        let mut blocks = HashMap::new();
        let mut lengths = HashMap::new();

        let h = block.hash();
        blocks.insert(h, block);
        lengths.insert(h, 0);

        debug!("{}/{} {:?}, tip {}", lengths[&h], blocks.len(), h, h);

        return Blockchain{tip: h, blocks, lengths};
    }

    /// Insert a block into blockchain
    pub fn insert(&mut self, block: &Block) {
        // Add block to self.blocks
        let h = block.hash();

        if self.blocks.contains_key(&h) {
            return;
        }
        self.blocks.insert(h, block.clone());

        // Assuming parent in self.blocks
        let parent = &block.header.parent;
        self.lengths.insert(h, self.lengths[&parent]+1);

        if self.lengths[&h] > self.lengths[&self.tip] {
            self.tip = h;
        }

        debug!("{}/{} {:?}, tip {}", self.lengths[&self.tip], self.blocks.len(), h, self.tip);
    }

    /// Get the last block's hash of the longest chain
    pub fn tip(&self) -> H256 {
        return self.tip;
    }

    /// Get the last block's hash of the longest chain
    #[cfg(any(test, test_utilities))]
    pub fn all_blocks_in_longest_chain(&self) -> Vec<H256> {
        unimplemented!()
    }
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::block::test::generate_random_block;
    use crate::crypto::hash::Hashable;

    #[test]
    fn insert_one() {
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        assert_eq!(blockchain.tip(), block.hash());

    }
}
