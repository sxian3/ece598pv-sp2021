use crate::network::server::Handle as ServerHandle;

use log::{debug, info};

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;

use std::thread;

// use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use crate::transaction::{self, Transaction, SignedTransaction, Mempool, State, StatePerBlock};
use crate::crypto::hash::{H160, H256, Hashable};
use crate::crypto::merkle::{MerkleTree};
use crate::block::{Block, Header, Content};
use crate::blockchain::Blockchain;
use rand::Rng;
use crate::network::message::Message;
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, UnparsedPublicKey, ED25519};
use bincode;

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Gen(u8, u8, u32),
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    mempool: Arc<Mutex<Mempool>>,
    states: Arc<Mutex<StatePerBlock>>,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    mempool: &Arc<Mutex<Mempool>>,
    states: &Arc<Mutex<StatePerBlock>>,
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        mempool: Arc::clone(mempool),
        states: Arc::clone(states),
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }

    pub fn gen(&self, from_in: u8, to_in: u8, value_in: u32) {
        self.control_chan.send(ControlSignal::Gen(from_in, to_in, value_in)).unwrap();
    }

    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }
}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                info!("Miner shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Start(i) => {
                info!("Miner starting in continuous mode with lambda {}", i);
                self.operating_state = OperatingState::Run(i);
            }
            ControlSignal::Gen(from_in, to_in, value) => {
                let _blockchain = self.blockchain.lock().unwrap();
                let mut _mempool = self.mempool.lock().unwrap();
                let _states = self.states.lock().unwrap();
                let state = _states.map[&_blockchain.tip].clone();
    
                let mut __from = [0u8; 32];
                __from[0] = from_in;
                let key_0 = Ed25519KeyPair::from_seed_unchecked(&__from).unwrap();
                let public_key_0 = key_0.public_key();
                let pkhash_0: H256 = ring::digest::digest(&ring::digest::SHA256, public_key_0.as_ref()).into();
                let from: H160 = pkhash_0.to_addr().into();
        
                let mut __to = [0u8; 32];
                __to[0] = to_in;
                let key_1 = Ed25519KeyPair::from_seed_unchecked(&__to).unwrap();
                let public_key_1 = key_1.public_key();
                let pkhash_1: H256 = ring::digest::digest(&ring::digest::SHA256, public_key_1.as_ref()).into();
                let to: H160 = pkhash_1.to_addr().into();
        
                let mut rng = rand::thread_rng();
                let nonce = state.state[&from].0 + 1;
                // let value = rng.gen_range(0, 10);
        
                let transaction = Transaction{from, nonce, to, value};
                let signature = transaction::sign(&transaction, &key_0);
                let t = SignedTransaction{transaction, signature: signature.as_ref().to_vec(), public_key: public_key_0.as_ref().to_vec()};
        
                let mut v = Vec::new();
                v.push(t.hash());
        
                _mempool.insert(&t);
                self.server.broadcast(Message::NewTransactionHashes(v));
            }
        }
    }

    fn miner_loop(&mut self) {
        // main mining loop
        let time_0 = time::Instant::now();
        let mut block_count = 0;
        let mut next_time = 15;
        let next_time_inc = 15;

        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            // actual mining
            let mut rng = rand::thread_rng();
            let mut _blockchain = self.blockchain.lock().unwrap();
            let mut _mempool = self.mempool.lock().unwrap();
            let mut _states = self.states.lock().unwrap();
            let mut state_new = _states.map[&_blockchain.tip].clone();
            
            let parent = _blockchain.tip();
            let nonce: u32 = rng.gen();
            let difficulty = _blockchain.blocks[&parent].header.difficulty;
            let timestamp = time::SystemTime::now().duration_since(time::SystemTime::UNIX_EPOCH).unwrap().as_millis();
            // let transactions = Vec::new();
            let mut txs = Vec::new();
            let block_size_limit = 2000;
            let mut block_size = 0;

            // Add transactions from mempool to merkle_tree
            for acc_nonce in _mempool.nonce_map.keys() {
                if acc_nonce.1 == state_new.state[&acc_nonce.0].0 + 1 {
                    let tx_hash = _mempool.nonce_map[&acc_nonce];
                    // serialize
                    let tx_clone = _mempool.tx_map[&tx_hash].clone();
                    let tx_se = bincode::serialize(&tx_clone).unwrap();
                    
                    // block size limit
                    block_size = block_size + tx_se.len();
                    if block_size > block_size_limit {
                        break;
                    }
                    
                    txs.push(tx_clone);
                    // transactions.push(tx_se);
                }
            }

            // new merkle tree from transactions
            let merkle_tree = MerkleTree::new(&txs);
            let merkle_root = merkle_tree.root();

            let header = Header{parent, nonce, difficulty, timestamp, merkle_root};
            let content = Content{content: txs.clone()};
            let block = Block{header, content};

            // BLOCK MINED
            if block.hash() <= difficulty {
                block_count = block_count + 1;

                let time_1 = time::Instant::now();
                if time_1.checked_duration_since(time_0).unwrap().as_secs() > next_time {
                    debug!("mined {}, hash = {:?}, {:?}", block_count, block.hash(), time_1.checked_duration_since(time_0));
                    next_time = next_time + next_time_inc;
                } else {
                    debug!("mined {}, hash = {:?}", block_count, block.hash());
                }

                // remove transaction from mempool and update state
                for t in &txs {
                    _mempool.remove(&t);
                    state_new.update(&t);
                }

                // insert block to blockchain and states
                _blockchain.insert(&block);
                debug!("{:?}", state_new);
                _states.map.insert(block.hash(), state_new);

                let mut v = Vec::new();
                v.push(block.hash());
                self.server.broadcast(Message::NewBlockHashes(v));
            }

            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }
        }
    }
}
