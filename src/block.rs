use serde::{Serialize, Deserialize};
use crate::crypto::hash::{H256, Hashable};
use crate::transaction::{Transaction, SignedTransaction};
use rand::Rng;
use std::time::{SystemTime, Duration};
use crate::crypto::merkle::{MerkleTree};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
    pub header: Header,
    pub content: Content,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Header {
    pub parent: H256,
    pub nonce: u32,
    pub difficulty: H256,
    pub timestamp: u128,
    pub merkle_root: H256
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Content {
    pub content: Vec<SignedTransaction>
}

// parent - a hash pointer to parent block. Please use H256 that we provide.
// nonce - a random integer that will be used in proof-of-work mining. We suggest to simply use u32.
// difficulty - the mining difficulty, i.e., the threshold in proof-of-work check. Please use H256 since we provide the comparison function, with which you can simply write if hash <= difficulty. (Proof-of-work check or mining is not required in this part.)
// timestamp - the timestamp when this block is generated. This is used to decide the delay of a block in future part of this project.
// merkle_root - the Merkle root of data (explained below in 6.).

impl Hashable for Block {
    fn hash(&self) -> H256 {
        return self.header.hash();
    }
}

impl Hashable for Header {
    fn hash(&self) -> H256 {
        let se_t = bincode::serialize(&self).unwrap();
        return ring::digest::digest(&ring::digest::SHA256, &se_t).into();
    }
}

#[cfg(any(test, test_utilities))]
pub mod test {
    use super::*;
    use crate::crypto::hash::H256;

    pub fn generate_random_block(parent: &H256) -> Block {
        let mut rng = rand::thread_rng();
        let nonce: u32 = rng.gen();

        let random_bytes: Vec<u8> = (0..32).map(|_| rng.gen()).collect();
        let mut raw_bytes = [0; 32];
        raw_bytes.copy_from_slice(&random_bytes);
        let difficulty = (&raw_bytes).into();

        let timestamp: u128 = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis();

        let transactions = Vec::<Transaction>::new();
        let merkle_tree = MerkleTree::new(&transactions);

        let header = Header{parent: *parent, nonce, difficulty, timestamp, merkle_root: merkle_tree.root()};
        let content = Content{content: Vec::new()};
        let block = Block{header, content};

        return block;
    }
}
