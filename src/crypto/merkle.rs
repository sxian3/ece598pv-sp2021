use super::hash::{Hashable, H256};
// use serde::{Serialize,Deserialize};
use ring::digest;
// use bincode;

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    nodes: Vec<H256>,
    num_leaves: Vec<usize>,
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        let mut vec = Vec::new();
        let mut vec_size = Vec::new();

        let mut size = data.len();

        if size != 0 {
            /* Push leaf nodes to vec */
            for block in data.iter() {
                vec.push(block.hash());
            }

            let mut flag = size % 2;
            if flag == 1 {
                vec.push(vec[vec.len()-1]);
                size += 1;
            }

            vec_size.push(size);

            /* Repeatedly construct the upper levels */
            let mut base = 0;
            let mut prev = size;
            let mut half = size / 2;

            while half > 0 {
                flag = half % 2; // determines if current calculated level has odd # of nodes

                for i in 0..half {
                    let mut ctx = digest::Context::new(&digest::SHA256);
                    ctx.update(vec[base + i * 2].as_ref());
                    ctx.update(vec[base + i * 2 + 1].as_ref());
                    let hashed = ctx.finish();
                    vec.push(hashed.into());
                }

                if flag == 1 && half != 1 {
                    vec.push(vec[vec.len()-1]);
                    half += 1;
                }

                vec_size.push(half);

                base += prev;
                prev = half;
                half = half / 2;
            }
        } else {
            vec.push([0u8; 32].into());
            vec_size.push(1);
        }

        // println!("{:?}", vec);

        return MerkleTree {nodes: vec, num_leaves: vec_size}
    }

    pub fn root(&self) -> H256 {
        return self.nodes[self.nodes.len()-1];
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut vec = Vec::new();
        let mut base = 0;
        let mut i = index;

        for num_nodes in &self.num_leaves[0..self.num_leaves.len()-1] {
            if i % 2 == 1 {
                println!("{} {} {}", num_nodes, index-1, self.nodes[base+i-1]);
                vec.push(self.nodes[base+i-1]);
            } else {
                println!("{} {} {}", num_nodes, index+1, self.nodes[base+i+1]);
                vec.push(self.nodes[base+i+1]);
            }
            base += num_nodes;
            i /= 2;
        }

        println!("self.nodes: {:?}", self.nodes);
        println!("vec: {:?}", vec);

        return vec;
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], _index: usize, _leaf_size: usize) -> bool {
    let mut hash = *datum;

    for p in 0..proof.len() {
        println!("{:?}", hash);
        let mut ctx = digest::Context::new(&digest::SHA256);
        ctx.update(hash.as_ref());
        ctx.update(proof[p].as_ref());
        let hashed = ctx.finish();
        hash = hashed.into();
    }

    return hash == *root;
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }

    macro_rules! gen_merkle_tree_assignment2 {
        () => {{
            vec![
                (hex!("0000000000000000000000000000000000000000000000000000000000000011")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000022")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000033")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000044")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000055")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000066")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000077")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000088")).into(),
            ]
        }};
    }

    #[test]
    fn assignment2_merkle_root() {
        let input_data: Vec<H256> = gen_merkle_tree_assignment2!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6e18c8441bc8b0d1f0d4dc442c0d82ff2b4f38e2d7ca487c92e6db435d820a10")).into()
        );
    }
}
